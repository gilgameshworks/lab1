# Lab1



## Getting started

Афанасьев Иван. Вариант 2.
Данные https://www.kaggle.com/c/bike-sharing-demand

Цель Работы:
Получить навыки разработки CI/CD pipeline для ML моделей с достижением 
метрик моделей и качества.

Ход Работы:

Так как данные табличные, было решено использовать xgboost модель.
Были оформлены базовые тесты и сделано базовое преобразование данных.
Для разработки CI/CD было решено использовать Teamcity, но в процессе разработки возникли проблемы с созданием agent. К сожалению, на моей windows системе не работали linux агенты. Тогда я создал linux агента через docker и попытался использовать его код, но уже не работал Teamcity сервер.
Пришлось отказаться от Teamcity и перейти на gitlab ci/cd разработку.
CI часть отвечает за сборку контейнера с помощью Dockerfile и отправки на dockerhub/
CD часть проводит тестирование модели.

Результаты Работы:
Работа была выполнена, нужные навыки получены.

