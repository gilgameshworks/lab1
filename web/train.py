import configparser
import os
import pandas as pd
import xgboost as xg
from preprocess import Preprocess
import logging


#train class and save weights
class Train():

    def __init__(self):
        self.filename = os.path.dirname(os.path.abspath(__file__))
        self.config = configparser.ConfigParser()
        config_path = self.filename + '/../config.ini'
        print(config_path)
        self.config.read(config_path)
        Prep = Preprocess()
        self.X = Prep.X
        self.Y = Prep.Y
        self.X_test = Prep.X_test
        logging.info(f'{self.__class__.__name__} is initialized')
        self.model_start(self.X, self.Y, self.X_test, self.config, self.filename)



    def model_start(self, X, Y, X_test, config, filename):
        #model train and save
        self.xgr=xg.XGBRegressor(max_depth=config['PARAMS']['max_depth'],
                            min_child_weight=config['PARAMS']['min_child_weight'], subsample=config['PARAMS']['subsample'], gamma=config['PARAMS']['gamma'],
                            colsample_bytree=config['PARAMS']['colsample_bytree'],) 

        self.xgr.fit(X,Y)
        self.xgr.save_model(filename + "/../" + config['MODEL']['path'])
        print('Model Saved')

#Test purposes
#B = Train()


  

