import configparser
import os
import pandas as pd
import xgboost as xg
from train import Train

import logging

#class to predict from model
class Predict():

    def __init__(self):
        self.filename = os.path.dirname(os.path.abspath(__file__))
        self.config = configparser.ConfigParser()
        config_path = self.filename + '/../config.ini'
        print(config_path)
        self.config.read(config_path)
        logging.info(f'{self.__class__.__name__} is initialized')
        self.tra = Train()
        

    def predict(self):
        y_output=self.tra.xgr.predict(self.tra.X_test)

        answer = pd.DataFrame({'count':(y_output)})
        answer.to_csv(self.filename + '/../results/sub2.csv')
        print('Predictions done')

#Test purposes
#B = Predict()
#B.predict()
#logging.debug('This message should go to the log file')
#logging.info('So should this')
#logging.warning('And this, too')
#logging.error('And non-ASCII stuff, too, like Øresund and Malmö')