import configparser
import os
import pandas as pd
import xgboost as xg
import logging


#class to preprocess data
class Preprocess():

    def __init__(self):
        filename = os.path.dirname(os.path.abspath(__file__))
        config = configparser.ConfigParser()
        config_path = filename + '/../config.ini'
        print(config_path)
        config.read(config_path)
        logging.info(f'{self.__class__.__name__} is initialized')
        self.X, self.Y, self.X_test = self.data_preparation(filename, config)
        print('Code Completed')

    def datetime_2_features(self, df):
        #creating new features from datetime
  
        df['year'] = df['datetime_c'].dt.year
        df['month'] = df['datetime_c'].dt.month
        df['day'] = df['datetime_c'].dt.day
        df['hour'] = df['datetime_c'].dt.hour
        df['min'] = df['datetime_c'].dt.minute
        df['sec'] = df['datetime_c'].dt.second
        df.drop(['datetime_c'], axis=1, inplace=True)

    def data_preparation(self,filename, config):
        #preparing data for training

        db_train = pd.read_csv(filename + '/../data_bike/train.csv')
        db_test = pd.read_csv(filename + '/../data_bike/test.csv')
        db_train.drop(['casual', 'registered'], axis=1, inplace=True)
        db_train['datetime_c'] =  pd.to_datetime(db_train['datetime'], format='%Y-%m-%d %H:%M:%S')
        db_test['datetime_c'] =  pd.to_datetime(db_test['datetime'], format='%Y-%m-%d %H:%M:%S')
        db_train.drop(['datetime'], axis=1, inplace=True)
        db_test.drop(['datetime'], axis=1, inplace=True)

        self.datetime_2_features(db_train)
        self.datetime_2_features(db_test)

        X=db_train.iloc[:,db_train.columns!='count'].values
        print(X[0])
        Y=db_train['count'].values
        X_test = db_test[:].values
        db_train.to_csv('../data_bike/prep_train.csv')
        db_test.to_csv('../data_bike/prep_test.csv')
        return X, Y, X_test

# Test purpose only
#A = Preprocess()