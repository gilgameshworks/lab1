import os
import traceback
#from logger import Logger

#from predict import Predictor
from flask import Flask, request, render_template
from predict import Predict
import numpy as np
import sys
import logging

logging.basicConfig(filename='example.log', level=logging.DEBUG)
SHOW_LOG = True

# init logger


# init flask

server = Flask(__name__)
server.debug = True

mdl = None
# loading classification model
try:
    print(1)#clf_model = Predictor()
except Exception:
    sys.exit(1)


@server.route('/', methods=['GET', 'POST'])
def index():

    page_context = None

    if request.method == 'POST':
        input_text = request.form['text']
        z = input_text.split(' ')
        z = [float(i) for i in z]
        a = np.array([z])
        page_context = {
            'input_text': input_text,
            'clf_result': mdl.predict(a)
        }


    return render_template('input_form.html', context=page_context)

if __name__ == "__main__":
   pred_cla = Predict()
   mdl = pred_cla.tra.xgr
   server.run()